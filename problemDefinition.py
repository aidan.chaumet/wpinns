import utils
import config as cfg


if cfg.what_solving == "sine":
    problemInterval = [-1.0,1.0] # Spatial Domain
    timeInterval = [0,1.0]
    initialCond = utils.initialCondSine
    soln = utils.solnSinePlot
    uL = 0 #uL and uR are the left and right boundary values in 1D
    uR = 0
else:
    problemInterval = [-1.0,1.0] # Spatial Domain
    timeInterval = [0,0.45]
    if cfg.what_solving == "shock_s":
        uL = 1.0
        uR = -1.0
        sv = (uL + uR)/2
        initialCond = lambda coordTens: utils.initialCondShock(coordTens,uL,uR)
        soln = lambda coord: utils.solnshockSHPlot(coord,sv,uL,uR)
    elif cfg.what_solving == "shock_m":
        uL = 1.0
        uR = 0.0
        sv = (uL + uR)/2
        initialCond = lambda coordTens: utils.initialCondShock(coordTens,uL,uR)
        soln = lambda coord: utils.solnshockSHPlot(coord,sv,uL,uR)
    else:
        uL = -1.0
        uR = 1.0
        initialCond = lambda coordTens: utils.initialCondShock(coordTens,uL,uR)
        soln = lambda coord: utils.solnrarPlot(coord,uL,uR)


