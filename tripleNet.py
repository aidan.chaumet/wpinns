import tensorflow as tf
from tensorflow import keras 
import numpy as np
import matplotlib.pyplot as plt
import datetime
import os
import time as tm
import scipy.stats.qmc as sobol
import problemDefinition as pd
import config as cfg
import utils

#--------------------------- Create Save and Logging Dirs
if cfg.logging:
    runName = cfg.runName
    if not os.path.exists(cfg.log_dir_base):
        os.makedirs(cfg.log_dir_base)
    if not os.path.exists(cfg.save_dir_base):
        os.makedirs(cfg.save_dir_base)
    if not os.path.exists(cfg.plot_dir_base):
        os.makedirs(cfg.plot_dir_base)

tf.keras.backend.set_floatx(cfg.prec)

tfPi = tf.constant(np.pi,dtype=cfg.prec)

# ------------------------- Network and Layer definition  ------------------------------
blockInd = 0
def resBlock(x,layerWidth,blkActFun,hasSkip):
    global blockInd
    blockInd += 1
    dense = keras.layers.Dense(layerWidth, activation=blkActFun,name=f"burgersBlock_{blockInd}",use_bias=True,kernel_initializer = "glorot_uniform")
    y = dense(x)
    blockInd += 1
    dense2 = keras.layers.Dense(layerWidth, activation=blkActFun,name=f"burgersBlock_{blockInd}",use_bias=True,kernel_initializer = "glorot_uniform")
    y = dense2(y)
    if(hasSkip):
        y = keras.layers.Add()([x, y])
    return y


class advBurgers(keras.Model):
    def __init__(self,solnNet=None,normNet=None,entNet=None,**kwargs):
        bvpen = None
        ivpen = None
        resMetric = 1.0e0
        if 'bvpen' in kwargs: # Storage for intial and boundary loss weights in serialized class when saving
            bvpen = (kwargs.pop('bvpen'))
        if 'ivpen' in kwargs: 
            ivpen = (kwargs.pop('ivpen'))
        if 'resMetric' in kwargs: #Storage for exponential loss average in serialized class when saving
            resMetric = (kwargs.pop('resMetric'))
        super(advBurgers,self).__init__(**kwargs)
        self.solnNet = solnNet #Network approximating Solution
        self.normNet = normNet #Network for PDE Loss estimation
        self.entNet = entNet   #Network for entropy loss estimation
        if bvpen is not None:
            self.setBV(bvpen)
        if ivpen is not None:
            self.setIV(ivpen)
        self.setResMetric(resMetric)

    def compile(self,s_opt=None,n_opt=None,e_opt=None,**kwargs):# Careful, loses opt state
                                                                # (Optimizer State is not saved with the model, so it is lost upon loading)
        super(advBurgers,self).compile(**kwargs)
        self.s_opt = s_opt
        self.n_opt = n_opt
        self.e_opt = e_opt
    
    def setBV(self,bvpen):
        self._penaltyBV = self.add_weight(name = "bvpen",dtype = cfg.prec,trainable = False)
        self._penaltyBV.assign(bvpen)
        
    def setIV(self,ivpen):
        self._penaltyIV = self.add_weight(name = "ivpen",dtype = cfg.prec,trainable = False)
        self._penaltyIV.assign(ivpen)

    def setResMetric(self,resMetric):
        self._resMetric = self.add_weight(name = "resMetric",dtype = cfg.prec,trainable = False)
        self._resMetric.assign(resMetric)
    
    @tf.function
    def updResMetric(self,newRes):
        self._resMetric.assign((1 - cfg.metricDecay)*self._resMetric + cfg.metricDecay * newRes)
        

    def setData(self,data):
        self.collocPoints = data

    def train_func(self,collocPoints):
        intPoints,ivPoints,bvPointsL,bvPointsR,fvPoints = self.collocPoints

        with tf.GradientTape(persistent = True) as trainingTape:
            #Compute Loss wrt. initial values (L^2 - based):
            predIVBG = self.solnNet(ivPoints,training=True)          
            realIV = pd.initialCond(ivPoints)
            ivLoss = tf.reduce_sum((predIVBG-realIV)*(predIVBG-realIV))*self._penaltyIV/tf.constant(cfg.numPointsIV,dtype=cfg.prec)

            bcLoss = ivLoss #Track different loss contributions separately for diagnostics

            #Compute Loss wrt. Boundary values (L^2 - based):
            predBVBG = self.solnNet(bvPointsL,training=True)-tf.constant(pd.uL,dtype=cfg.prec)
            bcLoss += tf.reduce_sum(predBVBG*predBVBG)*self._penaltyBV/tf.constant(cfg.numPointsBV,dtype=cfg.prec)
            predBVBG = self.solnNet(bvPointsR,training=True)-tf.constant(pd.uR,dtype=cfg.prec)
            bcLoss += tf.reduce_sum(predBVBG*predBVBG)*self._penaltyBV/tf.constant(cfg.numPointsBV,dtype=cfg.prec)

            loss = bcLoss

            #Compute derivatives u_t and entropy parts d_t(u²/2) and d_x(entropyFun) of the entroy flux
            # Note that u²/2 is our entropy and u³/3 is the entropy flux
            # Because of the integration by parts in the residual we do not need d_x(u²/2)
            with tf.GradientTape(persistent=True) as netDerivTape:
                (time,space) = tf.unstack(intPoints,axis=1)
                netDerivTape.watch(time)
                netDerivTape.watch(space)
                helperPoints = tf.stack([time,space],axis=1)
                predIntBG = self.solnNet(helperPoints,training=True)               
                predIntSq = tf.constant(0.5,dtype=cfg.prec)*predIntBG*predIntBG
                entropyFun = predIntBG*predIntBG*predIntBG/3.0
            batchGradsTime =  tf.reshape(netDerivTape.gradient(predIntBG,time),shape=predIntBG.shape)
            entropyDerivX = netDerivTape.gradient(entropyFun,space)
            entropyDerivT = netDerivTape.gradient(predIntSq,time)
            del netDerivTape

            # Query adversarial net for estimate to dual problem solution
            # We need -|grad_x adversarial|² so we do this in a gradient tape
            with tf.GradientTape(persistent=True) as advTape:
                (time,space) = tf.unstack(intPoints,axis=1)
                advTape.watch(time)
                advTape.watch(space)
                helperPoints = tf.stack([time,space],axis=1)
                predIntNN = self.normNet(helperPoints,training=True)*utils.cutoff(helperPoints)  
            gradsNN = tf.reshape(advTape.gradient(predIntNN,space),shape = predIntSq.shape)

            area = (pd.timeInterval[1]-pd.timeInterval[0])*(pd.problemInterval[1]-pd.problemInterval[0])
            integrand = batchGradsTime*predIntNN - predIntSq*gradsNN
            # Assemble interior loss functional min_R max _v int_t int_x R*v - 0.5*|g_x adv|²
            # We can leave out -0.5*|g_x adv| here because it does not give any update to solution net weights (independent gradient)
            resLoss = tf.reduce_sum(integrand)/integrand.shape[0]*area
            loss += resLoss

            predIntEnt = self.entNet(intPoints)*utils.cutoff(intPoints)
            entropyRHS = tf.reshape(tf.keras.activations.relu(entropyDerivT + entropyDerivX),shape = predIntEnt.shape)
            entropy = tf.reduce_sum(entropyRHS*predIntEnt)/entropyRHS.shape[0]*area

            loss +=entropy*cfg.entropyPenalty

        funcTrainVars = self.solnNet.trainable_weights
        funcGrads = trainingTape.gradient(loss,funcTrainVars)
        self.s_opt.apply_gradients(zip(funcGrads,funcTrainVars))
        return {"loss":loss,"bcLoss":bcLoss,"resLoss": resLoss, "entropy":entropy}

    def train_norm_ent(self,collocPoints):
        intPoints,ivPoints,bvPointsL,bvPointsR,fvPoints = self.collocPoints

        with tf.GradientTape(persistent = True) as trainingTape:
            loss = 0.0

            #Compute derivatives u_t and entropy parts d_t(u²/2) and d_x(entropyFun) of the entroy flux
            # Note that u²/2 is our entropy and u³/3 is the entropy flux
            # Because of the integration by parts in the residual we do not need d_x(u²/2)
            with tf.GradientTape(persistent=True) as netDerivTape:
                (time,space) = tf.unstack(intPoints,axis=1)
                netDerivTape.watch(time)
                netDerivTape.watch(space)
                helperPoints = tf.stack([time,space],axis=1)
                predIntBG = self.solnNet(helperPoints,training=True)               
                predIntSq = tf.constant(0.5,dtype=cfg.prec)*predIntBG*predIntBG
                entropyFun = predIntBG*predIntBG*predIntBG/3.0
            batchGradsTime =  tf.reshape(netDerivTape.gradient(predIntBG,time),shape=predIntBG.shape)
            entropyDerivX = netDerivTape.gradient(entropyFun,space)
            entropyDerivT = netDerivTape.gradient(predIntSq,time)
            del netDerivTape
            
            # Query adversarial net for estimate to dual problem solution
            # We need -|grad_x adversarial|² so we do this in a gradient tape

            with tf.GradientTape(persistent=True) as advTape:
                (time,space) = tf.unstack(intPoints,axis=1)
                advTape.watch(time)
                advTape.watch(space)
                helperPoints = tf.stack([time,space],axis=1)
                predIntNN = self.normNet(helperPoints,training=True)*utils.cutoff(helperPoints)
            gradsNN = tf.reshape(advTape.gradient(predIntNN,space),shape = predIntSq.shape)
            del advTape

            area = (pd.timeInterval[1]-pd.timeInterval[0])*(pd.problemInterval[1]-pd.problemInterval[0])
            integrand = batchGradsTime*predIntNN - predIntSq*gradsNN
            # Assemble interior loss functional min_R max _v int_t int_x R*v - 0.5*|g_x adv|²
            resLoss = tf.reduce_sum(integrand)/integrand.shape[0]*area
            
            gradLoss = -tf.constant(0.5,dtype=cfg.prec)*tf.reduce_sum(gradsNN*gradsNN)/integrand.shape[0]*area
            
            loss += resLoss
            loss += gradLoss

            # Gradients of entropy dual network
            with tf.GradientTape(persistent=True) as entTape:
                (time,space) = tf.unstack(intPoints,axis=1)
                entTape.watch(time)
                entTape.watch(space)
                helperPoints = tf.stack([time,space],axis=1)
                predIntEnt = self.entNet(helperPoints,training=True)*utils.cutoff(helperPoints)
            gradsEnt = tf.reshape(entTape.gradient(predIntEnt,space),shape = predIntSq.shape)
            del entTape

            entropyRHS = tf.reshape(tf.keras.activations.relu(entropyDerivT + entropyDerivX),shape = predIntEnt.shape)
            entRes = tf.reduce_sum(entropyRHS*predIntEnt)/entropyRHS.shape[0]*area
            entGrad = -tf.constant(0.5,dtype=cfg.prec)*tf.reduce_sum(gradsEnt*gradsEnt)/entropyRHS.shape[0]*area

            loss2 = entRes + entGrad
        
        normTrainVars = self.normNet.trainable_weights
        normGrads = trainingTape.gradient(loss,normTrainVars)
        self.n_opt.apply_gradients(zip([-x for x in normGrads],normTrainVars))
        entTrainVars = self.entNet.trainable_weights
        entGrads = trainingTape.gradient(loss2,entTrainVars)
        self.e_opt.apply_gradients(zip([-x for x in entGrads],entTrainVars))
        return {"resLoss": resLoss, "gradLoss":gradLoss,"entRes":entRes,"entGrad":entGrad}

    def train_step(self,collocPoints):
        for _ in range(cfg.numNormSteps):
            normDict = self.train_norm_ent(collocPoints)
        funcDict = self.train_func(collocPoints)
        totalLoss = funcDict["loss"] + normDict["gradLoss"] + normDict["entGrad"]
        #Update exponential-smoothed training metric
        self.updResMetric(funcDict["bcLoss"] + funcDict["resLoss"]+  funcDict["entropy"])
        return {"loss":totalLoss,"bcLoss":funcDict["bcLoss"],"resLoss": funcDict["resLoss"], "gradLoss":normDict["gradLoss"],
                                                             "entGrad":normDict["entGrad"],"entropy":funcDict["entropy"], "resMetric": self._resMetric}

    def get_config(self):
        #Note: This currently loses optimizer state
        config = super().get_config()
        solnCfg = self.solnNet.get_config()
        advCfg = self.normNet.get_config()
        entCfg = self.entNet.get_config()
        config["solnCfg"] = solnCfg
        config["advCfg"] = advCfg
        config["entCfg"] = entCfg
        config["_penaltyBV"] = self._penaltyBV.numpy()
        config["_penaltyIV"] = self._penaltyIV.numpy()
        config["_resMetric"] = self._resMetric.numpy()
        return config

    #From #https://github.com/keras-team/keras/blob/v2.9.0/keras/engine/training.py#L67-L3266
    @classmethod
    def from_config(cls,config,custom_objects = None):
        print("Called fromConfig")
        print("---------------")
        print("---------------")
        print("---------------")
        print("---------------")
        bvpen = None
        ivpen = None
        resMetric = 1.0e0
        print(config["_penaltyBV"])
        if '_penaltyBV' in config: 
            bvpen = (config.pop('_penaltyBV'))
        if '_penaltyIV' in config: 
            ivpen = (config.pop('_penaltyIV'))
        if '_resMetric' in config: 
            resMetric = (config.pop('_resMetric'))
        print("Got here")
        print("bvpen:",bvpen)
        print("ivpen:",ivpen)
        print("resMetric",resMetric)
    # `from_config` assumes `cls` is either `Functional` or a child class of
    # `Functional`. In the case that `cls` is meant to behave like a child class
    # of `Functional` but only inherits from the `Model` class, we have to call
    # `cls(...)` instead of `Functional.from_config`.
        from keras.engine import functional  # pylint: disable=g-import-not-at-top
        from keras.utils import generic_utils
        def reconFromCFG(config):
            with generic_utils.SharedObjectLoadingScope():
                functional_model_keys = [
                    'name', 'layers', 'input_layers', 'output_layers'
                ]
                if all(key in config for key in functional_model_keys):
                    inputs, outputs, layers = functional.reconstruct_from_config(
                        config, custom_objects)
                    print(inputs)
                    model = keras.Model(inputs=inputs, outputs=outputs, name=config.get('name'))
                    functional.connect_ancillary_layers(model, layers)
            return model,inputs,outputs
        advNet,_,_ = reconFromCFG(config.pop('advCfg'))
        entNet,_,_ = reconFromCFG(config.pop('entCfg'))
        solnNet,inputs,outputs = reconFromCFG(config.pop('solnCfg'))
        model = advBurgers(inputs=inputs, outputs=outputs,solnNet = solnNet,normNet = advNet,entNet = entNet,name=config["name"])
        if bvpen is not None:
            model.setBV(bvpen)
        if ivpen is not None:
            model.setIV(ivpen)
        model.setResMetric(resMetric)
        return model



#--------------------------------------------- Main Program for command line------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == "__main__":
    #---------------------------Building Model Layout and Optimizer ------------------------------
    inputs = keras.Input(shape=(2,))

    z1 = resBlock(inputs,cfg.funcLayerWidth,cfg.funcBlkActFun,cfg.funcHasSkip) # Note that if hasSkip is false, 
    z2 = resBlock(inputs,cfg.advLayerWidth,cfg.advBlkActFun,cfg.advHasSkip)    # Then resBlock just produces two normal layers
    z3 = resBlock(inputs,cfg.entLayerWidth,cfg.entBlkActFun,cfg.entHasSkip)

    for i in range(cfg.funcBlockNum-1):
        z1 = resBlock(z1,cfg.funcLayerWidth,cfg.funcBlkActFun,cfg.funcHasSkip)

    for i in range(cfg.advBlockNum-1):
        z2 = resBlock(z2,cfg.advLayerWidth,cfg.advBlkActFun,cfg.advHasSkip)
    
    for i in range(cfg.entBlockNum-1):
        z3 = resBlock(z3,cfg.entLayerWidth,cfg.entBlkActFun,cfg.entHasSkip)

    outFunc = keras.layers.Dense(1,name="BurgersOutput")(z1)
    outAdv  = keras.layers.Dense(1,name="advOutput")(z2)
    outEnt  = keras.layers.Dense(1,name="entOutput")(z3)
    funcNet = keras.Model(inputs = inputs,outputs = outFunc)
    advNet = keras.Model(inputs = inputs,outputs = outAdv)
    entNet = keras.Model(inputs = inputs,outputs = outEnt)

    #Put all three separate fully connected layers into a "container" class for training
    model = advBurgers(inputs = inputs,outputs = outFunc,name = "totalNet",solnNet = funcNet,
                    normNet = advNet,entNet = entNet,ivpen = cfg.penaltyIV,bvpen = cfg.penaltyBV)
    
    model.setBV(cfg.penaltyBV)
    model.setIV(cfg.penaltyIV)

    # --------------Optimizers --------------------------------
    funcLrSched = tf.keras.optimizers.schedules.PolynomialDecay(cfg.funcLRate,int(cfg.epochNum*cfg.epochCycles),
    end_learning_rate=0.5*cfg.funcLRate,
    power=1.0,
    cycle=False,
    name=None)
    advLrSched = tf.keras.optimizers.schedules.PolynomialDecay(cfg.advLRate,int(cfg.epochNum*cfg.epochCycles),
    end_learning_rate=0.5*cfg.advLRate,
    power=1.0,
    cycle=False,
    name=None)
    entLrSched = tf.keras.optimizers.schedules.PolynomialDecay(cfg.entLRate,int(cfg.epochNum*cfg.epochCycles),
    end_learning_rate=0.5*cfg.entLRate,
    power=1.0,
    cycle=False,
    name=None)
    opt1 = keras.optimizers.Adam(learning_rate = funcLrSched,amsgrad=True,epsilon=1e-8)
    opt2 = keras.optimizers.Adam(learning_rate = advLrSched,amsgrad=True,epsilon=1e-8)
    opt3 = keras.optimizers.Adam(learning_rate = entLrSched,amsgrad=True,epsilon=1e-8)

    if cfg.load == True: #Prompt for loading model from file
        model = None
        loadPath = input("Input path to save file:\n")
        model = keras.models.load_model(loadPath,custom_objects={"advBurgers": advBurgers,"sin":tf.math.sin})
    model.compile(opt1,opt2,opt3,run_eagerly=False) #eager execution only for debugging, much slower
    funcNet.summary()
    advNet.summary()
    entNet.summary()
    model.summary()
    #input()
    

    # Make collocation points
    intPoints = utils.intPointDomain(cfg.numPointsInt)
    ivPoints = utils.IVPointsDomain(cfg.numPointsIV)
    bvPointsL = utils.BVPointsLeft(int(cfg.numPointsBV/2))
    bvPointsR = utils.BVPointsRight(int(cfg.numPointsBV/2))
    fvPoints = utils.fixedTimeDomain(cfg.numPointsIV,pd.timeInterval[1])#Currently unused
    collocPoints = [intPoints,ivPoints,bvPointsL,bvPointsR,fvPoints]
    model.setData(collocPoints)
    xData = [[1,1]]
    if cfg.logging:
        log_dir_run = cfg.log_dir_base + "/"+ runName + "-"+  datetime.datetime.now().strftime("%H%M%S")
        plot_dir_run = cfg.plot_dir_base + "/"+ runName + "-"+  datetime.datetime.now().strftime("%H%M%S")
        save_dir_run = cfg.save_dir_base + "/"+ runName + "-"+  datetime.datetime.now().strftime("%H%M%S")
        if not os.path.exists(log_dir_run):
            os.makedirs(log_dir_run)
        if not os.path.exists(plot_dir_run):
            os.makedirs(plot_dir_run)
        if not os.path.exists(save_dir_run):
            os.makedirs(save_dir_run)
        with open(log_dir_run +"/metadata.txt", "w") as myfile:
            myfile.write('\n')
            myfile.write(f"prec: {cfg.prec}\n")
            myfile.write(f"save/load: {cfg.save},{cfg.load}\n")
            if cfg.load:
                myfile.write(f"loaded from {loadPath}\n")
            else:
                myfile.write(f"Model design parameters:\n")
                myfile.write(f"Solution Network:\n")
                myfile.write(f"blockWidth: {cfg.funcLayerWidth}, finalWidth:{cfg.funcFlLayerWidth}, blockNum: {cfg.funcBlockNum}\n")
                myfile.write(f"blkAct:{cfg.funcBlkActFun},inputAct:{cfg.funcInputAct},finalAct:{cfg.funcFinalAct}\n")
                myfile.write(f"Learning rate: {cfg.funcLRate}\n")
                myfile.write(f"Adversarial Networks:\n")
                myfile.write(f"blockWidth: {cfg.advLayerWidth}, finalWidth:{cfg.advFlLayerWidth}, blockNum: {cfg.advBlockNum}\n")
                myfile.write(f"blkAct:{cfg.advBlkActFun},inputAct:{cfg.advInputAct},finalAct:{cfg.advFinalAct}\n")
                myfile.write(f"Learning rate: {cfg.advLRate}\n")
                myfile.write(f"Training Details:\n")
                myfile.write(f"penaltyIV: {model._penaltyIV},penaltyBV: {model._penaltyBV}\n")
                myfile.write(f"Interior Points: {cfg.numPointsInt},Boundary Points: {cfg.numPointsBV},Initial Points: {cfg.numPointsIV}\n")
                myfile.write(f"Number of Max Steps per min: {cfg.numNormSteps}\n")
    timeTotal = 0
    if cfg.logging:
        chkp = tf.keras.callbacks.ModelCheckpoint(save_dir_run+"/bestModel{epoch}",monitor="resMetric",verbose=cfg.verboseOutput,
                                                      save_best_only = True,
                                                      mode = "min",
                                                      save_freq = cfg.chkp_freq,
                                                      initial_value_threshhold = 1.0)

    for ep in range(cfg.epochCycles):
        startEp = tm.time()
        if cfg.logging:
            log_dir = log_dir_run+ "/"+ f"ep{ep}"
            tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir,histogram_freq=500) #Advanced log creation for tensorboard
            #Model Checkpoint callback for optimal model
            history = model.fit(xData,None,batch_size = cfg.batchSz, epochs = cfg.epochNum*(ep+1), initial_epoch = cfg.epochNum*ep,
                                                     verbose=cfg.verboseOutput, callbacks = [tensorboard_callback,chkp])
            epTime = tm.time() - startEp
            timeTotal += epTime
        else:
            history = model.fit(xData,None,batch_size = cfg.batchSz, epochs = cfg.epochNum, verbose=cfg.verboseOutput)
            print(0)
        print(f"Total training time: {timeTotal}")
        #-----------------------Plotting and Saving of Models---------------------------
        xValidate = utils.intPointDomain(cfg.valNum)
        
        yPredict = model.predict(xValidate)
        yPredict = tf.convert_to_tensor(yPredict)
        yPredict = tf.reshape(yPredict,shape= (xValidate.shape[0],1))
        yReal =  [pd.soln(x) for x in xValidate.numpy()]
        yReal = tf.convert_to_tensor(yReal,dtype = cfg.prec)
        yReal = tf.reshape(yReal,shape = (xValidate.shape[0],1))
        
        l2ErrIt = tf.math.sqrt(tf.reduce_sum((yReal-yPredict)**2)/tf.reduce_sum(yReal**2))
        print("Relative L2 error:",l2ErrIt.numpy())
        l1ErrIt = tf.reduce_sum(tf.math.abs(yReal-yPredict))/tf.reduce_sum(tf.math.abs(yReal))
        print("Relative L1 error:",l1ErrIt.numpy())
        if cfg.plotting:
            callable = lambda x: pd.soln(x) - model(x)
            
            xValidate = utils.intPointDomain(cfg.valNum)
            yPredict = model.predict(xValidate)
            yPredict = tf.convert_to_tensor(yPredict)
            yPredict = tf.reshape(yPredict,shape= (xValidate.shape[0],1))
            yReal =  [pd.soln(x) for x in xValidate.numpy()]
            yReal = tf.convert_to_tensor(yReal,dtype = cfg.prec)
            yReal = tf.reshape(yReal,shape = (xValidate.shape[0],1))
            
            fig1 = plt.figure()
            #plt.scatter(xValidate[:,0],xValidate[:,1])
            ax1 = fig1.add_subplot(1,2,1,projection = "3d")
            ax1.set_title("$u_\\theta$")
            ax3 = fig1.add_subplot(1,2,2,projection="3d")
            ax3.set_title("$u - u_\\theta$")
            tcf = ax1.scatter(xValidate[:,0],xValidate[:,1],yPredict[:,0],c = yPredict, cmap = plt.cm.viridis)
            tcf2 = ax3.scatter(xValidate[:,0],xValidate[:,1],yReal-yPredict, c= yReal-yPredict, cmap = plt.cm.viridis)
            plt.colorbar(tcf,ax = ax1)
            plt.colorbar(tcf2,ax=ax3)
            fig1.set_figwidth(15)        

            fig2 = plt.figure()
            fig2.set_figwidth(15)
            sliceTimes = [0.0,0.1,0.25,0.35,0.45]
            for i,t in enumerate([x for x in sliceTimes]):
                fig2.add_subplot(2,3,i+1)
                xPlot = utils.fixedTimeDomain(cfg.plotNum,t)
                order = np.argsort(xPlot[:,1])
                yPlot= model.predict(xPlot)
                yPlot = tf.convert_to_tensor(yPlot)
                yPlot = tf.reshape(yPlot,shape= (xPlot.shape[0],1))
                yReal =  [pd.soln(x) for x in xPlot.numpy()]
                yReal = tf.convert_to_tensor(yReal,dtype = cfg.prec)
                yReal = tf.reshape(yReal,shape = (xPlot.shape[0],1))
                plt.title(f"Time slice {t}")
                plt.plot(xPlot[:,1].numpy()[order],yPlot.numpy()[order],marker="o",markersize = 2)
                plt.plot(xPlot[:,1].numpy()[order],yReal.numpy()[order],"r--")
            
            
            tdSlicePlt = plt.figure()
            tdSlicePlt.set_figwidth(15)
            for i,t in enumerate([x for x in sliceTimes]):
                tdSlicePlt.add_subplot(2,3,i+1)
                xPlot = utils.fixedTimeDomain(cfg.plotNum,t)
                order = np.argsort(xPlot[:,1])
                with tf.GradientTape(persistent=True) as netDerivTape:
                    (time,space) = tf.unstack(xPlot,axis=1)
                    netDerivTape.watch(time)
                    netDerivTape.watch(space)
                    helperPoints = tf.stack([time,space],axis=1)
                    predIntBG = model(helperPoints,training=False)               
                    #predIntSq = tf.constant(0.5,dtype=prec)*predIntBG*predIntBG
                yPlot = tf.reshape(netDerivTape.gradient(predIntBG,time),(cfg.plotNum,1))          
                plt.title(f"Time slice, $u_t$ {t}")
                plt.plot(xPlot[:,1].numpy()[order],yPlot.numpy()[order])
            
            xdSlicePlt = plt.figure()
            xdSlicePlt.set_figwidth(15)
            for i,t in enumerate([x for x in sliceTimes]):
                xdSlicePlt.add_subplot(2,3,i+1)
                xPlot = utils.fixedTimeDomain(cfg.plotNum,t)
                order = np.argsort(xPlot[:,1])
                with tf.GradientTape(persistent=True) as netDerivTape:
                    (time,space) = tf.unstack(xPlot,axis=1)
                    netDerivTape.watch(time)
                    netDerivTape.watch(space)
                    helperPoints = tf.stack([time,space],axis=1)
                    predIntBG = model(helperPoints,training=False)               
                    predIntSq = tf.constant(0.5,dtype=cfg.prec)*predIntBG*predIntBG
                yPlot = tf.reshape(netDerivTape.gradient(predIntSq,space),(cfg.plotNum,1))   
                plt.title(f"Time slice, $u_x$ {t}")
                plt.plot(xPlot[:,1].numpy()[order],yPlot.numpy()[order])
            resSlicePlt = plt.figure()
            resSlicePlt.set_figwidth(15)
            for i,t in enumerate([x for x in sliceTimes]):
                resSlicePlt.add_subplot(2,3,i+1)
                xPlot = utils.fixedTimeDomain(cfg.plotNum,t)
                order = np.argsort(xPlot[:,1])
                with tf.GradientTape(persistent=True) as netDerivTape:
                    (time,space) = tf.unstack(xPlot,axis=1)
                    netDerivTape.watch(time)
                    netDerivTape.watch(space)
                    helperPoints = tf.stack([time,space],axis=1)
                    predIntBG = model(helperPoints,training=False)               
                    predIntSq = tf.constant(0.5,dtype=cfg.prec)*predIntBG*predIntBG
                yPlot = tf.reshape(netDerivTape.gradient(predIntBG,time),(cfg.plotNum,1))+tf.reshape(netDerivTape.gradient(predIntSq,space),(cfg.plotNum,1))        
                plt.title(f"Time slice, point res. {t}")
                plt.plot(xPlot[:,1].numpy()[order],yPlot.numpy()[order])
               
        if cfg.logging:    
            if cfg.plotting:
                plot_dir = plot_dir_run+ "/"+f"ep{ep}"
                if not os.path.exists(plot_dir):
                    os.makedirs(plot_dir)
                #print(plot_dir+"/3dplotshockBig.png")
                fig1.savefig(plot_dir+"/3dplotshockBig.png",dpi=400)
                fig2.savefig(plot_dir+"/sliceplotshockBig.png",dpi=400)                
                tdSlicePlt.savefig(plot_dir+"/sliceplotdtRes.png",dpi=400)
                xdSlicePlt.savefig(plot_dir+"/sliceplotdxRes.png",dpi=400)
                resSlicePlt.savefig(plot_dir+"/sliceplotRes.png",dpi=400)
        if cfg.logging:
            with open(log_dir_run+"/lerrs.txt", "a") as myfile:
                myfile.write('\n')
                out = f"Epoch: {ep}, L2 Error:{l2ErrIt}, L1 Error:{l1ErrIt}"
                myfile.write(out)
        #plt.show()
        if cfg.logging:            
            if cfg.save == True:
                if not os.path.exists(save_dir_run):
                    os.makedirs(save_dir_run)
                model.save(save_dir_run+ "/"+f"/W1inf_adv_{ep}") 
       
        #plt.show()
        plt.close("all")

