# Efficient wPINNs https://arxiv.org/abs/2211.12393
This repo contains the companion code to our recent paper "Efficient wPINN-Approximations to Entropy Solutions of Hyperbolic Conservation Laws".

Abstract:
``` We consider the approximation of weak solutions of nonlinear hyperbolic PDEs using neural networks, similar to the classical PINNs approach, but using a weak (dual) norm of the residual. This is a variant of what was termed "weak PINNs" recently. We provide some explicit computations that highlight why classical PINNs will not work well for discontinuous solutions to nonlinear hyperbolic conservation laws and we suggest some modifications to the weak PINN methodology that lead to more efficient computations and smaller errors.```


## Repo Contents

- [ ] config.py: Contains all network-related , training-related and general hyperparameters to configure for simulations.
- [ ] problemDefinition.py: Contains PDE-specific information like domain geometry and initial/boundary conditions. A list of pre-defined problems is available.
- [ ] utils.py: Contains several miscellaneous functions.
- [ ] tripleNet.py: Main neural network training and testing code. Can be run from the command line without additional arguments after setting run details in config.py
- [ ] sineData: Folder containing data for a reference solution of the sine example problem
