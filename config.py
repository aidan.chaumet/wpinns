import datetime
#--------------------------- Logging and Plotting
logging = False
plotting = False
verboseOutput = 1 # Controls verbosity level of console output

#------------ Names for Save/Log/Plot directories
runName = "Rarefac_tanh_3000_new"
log_dir_base = "../logs/fit/" + datetime.datetime.now().strftime("%Y%m%d")
save_dir_base = "../savedModels/" + datetime.datetime.now().strftime("%Y%m%d")
plot_dir_base = "../Plots/Burgers1D/" + datetime.datetime.now().strftime("%Y%m%d")

# ---------------------------- Misc Constants
prec = "float32" #Tensorflow float precision
save = True # Whether to save trained models and model checkpoints 
load = False # Load existing Model from file. Triggers prompt in terminal for path to saved model
what_solving = "shock_s" # Options: shock_s, shock_m, rar, sine

#-------------------Model Design Variables--------------------------------
funcLayerWidth = 20  #Layer Sizes
funcFlLayerWidth = 20
advLayerWidth = 10
advFlLayerWidth = 10
entLayerWidth = 10
entFlLayerWidth = 10
funcHasSkip = False #Optional Flag for Skip connections as in ResNets
funcBlockNum = 3    # A Block comes as two layers, so this is half the number of desired layers
advHasSkip = False
advBlockNum = 2
entHasSkip = False
entBlockNum = 2

funcBlkActFun = "tanh" # Activation Functions
funcInputAct = "tanh"
funcFinalAct = "tanh"
advBlkActFun = "tanh"
advInputAct = "linear"
advFinalAct = "tanh"
entBlkActFun = "tanh"
entInputAct = "linear"
entFinalAct = "tanh"
funcLRate = 0.01 # Learning Rates
advLRate = 0.015
entLRate = 0.015
numNormSteps = 8 # n_max from our paper


#------------------ Training details--------------------------------
numPointsIV = 4096 # Number of points to sample from initial condition
numPointsBV = 4096 # Number of points to sample from boundary conditions
numPointsInt = 16384 #Number of interior collocation points
type = "MC" #What type of random points, options are "MC" and "sobol"
batchSz = 100000000  #Minibatch size
penaltyIV = 1.0e0 #Weight \lambda for inital- and boundary condition weighting in loss 
penaltyBV = 1.0e0
entropyPenalty = 1.0e0 # Optional weight for entropy loss part (no weighting described in paper)

epochNum = 200 # Number of steps per epoch
epochCycles = 5 # Number of epochs
                 # epochNum*epochCycles is the total number of training steps done,
                 # The split into these two quantities is to break up the control flow
                 # to track metrics at intermediate points during training


metricDecay = 0.3 # This is gamma for checkpointing
chkp_freq = 40 # For performance reasons we do not save model checkpoints every epoch
               # This is the frequency at which checkpoints may be saved

#--------------------Plotting and Validation details--------------------------
valNum = 2**17 # Number of validation points (for L^1 error computations)
plotNum = 6000# For 1D-Plots