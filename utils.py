import tensorflow as tf
import numpy as np
import config as cfg
import problemDefinition as pd
from scipy.interpolate import interp2d
import scipy.stats.qmc as sobol

@tf.function
def initialCondShock(coordTens,uL,uR):
    mask1 = tf.cast(coordTens[:,1] < 0,dtype=cfg.prec)*tf.constant(uL, dtype=cfg.prec)
    mask2 = tf.cast(coordTens[:,1] >=0,dtype=cfg.prec)*tf.constant(uR,dtype=cfg.prec)
    return tf.reshape(mask1 + mask2,(coordTens.shape[0],1))

def initialCondSine(coordTens):
    return tf.reshape(-tf.math.sin(coordTens[:,1]*np.pi),shape=(coordTens.shape[0],1))

def solnshockSHPlot(coord,sv,uL,uR):
    t,x = coord
    if x < sv*t:
        return uL
    if x >= sv*t:
        return uR
    return 0

def solnrarPlot(coord,uL,uR):
    t,x = coord
    if x < uL*t:
        return uL
    if x >= uR*t:
        return uR
    else:
        return uL + (x-uL*t)*(uR-uL)/(uR*t - uL*t)
    return 0


def cutoff(coordTens): #Quadratic Cutoff for enforcing boundary conditions
    x_mod = (coordTens[:,1] - (pd.problemInterval[0]+pd.problemInterval[1])/2.)/((pd.problemInterval[1]-pd.problemInterval[0])/2)
    return tf.reshape((x_mod**2 - 1.0),shape = (coordTens.shape[0],1))

# ------------------ Reference solution for sine problem
timeList = []
data = []
for i in range(1000):
    rowData = np.loadtxt("./sineData/_output/fort.q"+f"{i:04}",skiprows=6)
    timeList.append(i*0.001)
    data.append(rowData)
head =  np.loadtxt("./sineData/_output/fort.q"+f"{i:04}",max_rows = 6,usecols=0)
xList = [head[4]*step+head[3] for step in range(int(head[2]))]
data = np.asarray(data)
timeList = np.asarray(timeList)
xList = np.asarray(xList) 
exact = interp2d(timeList,xList,np.transpose(data))

def solnSinePlot(coord):
    t,x = coord
    return exact(t,x)

# -------------------------- Point Generation for Domain -----------------------
@tf.function
def intPointDomainMC(numPoints): #Generate interior points in the space-time domain
    time = tf.random.uniform(shape= (numPoints,),minval=pd.timeInterval[0],maxval=pd.timeInterval[1],dtype=cfg.prec)
    space = tf.random.uniform(shape= (numPoints,),minval=pd.problemInterval[0],maxval=pd.problemInterval[1],dtype=cfg.prec)
    return tf.stack([time,space],axis=1)

sobolGen = sobol.Sobol(2)
def intPointDomainSobol(numPoints):
    randPoints = sobolGen.random(numPoints)
    randPoints[:,0] = randPoints[:,0]*(pd.timeInterval[1]-pd.timeInterval[0]) + pd.timeInterval[0]
    randPoints[:,1] = randPoints[:,1]*(pd.problemInterval[1]-pd.problemInterval[0]) + pd.problemInterval[0]
    return tf.constant(randPoints,dtype = cfg.prec)
if cfg.type == "MC":
    intPointDomain = intPointDomainMC
if cfg.type == "sobol":
    intPointDomain = intPointDomainSobol

def IVPointsDomain(numPoints): #Generate points for the inital condition
    time = tf.ones(shape=(numPoints,),dtype=cfg.prec)*tf.constant(pd.timeInterval[0],dtype=cfg.prec)
    space = tf.random.uniform(shape=(numPoints,),minval=pd.problemInterval[0],maxval=pd.problemInterval[1],dtype=cfg.prec)
    return tf.stack([time,space],axis=1)

def fixedTimeDomain(numPoints,time): #Generate points in space at a fixed time
    time = tf.ones(shape=(numPoints,),dtype=cfg.prec)*tf.constant(time,dtype=cfg.prec)
    space = tf.random.uniform(shape=(numPoints,),minval=pd.problemInterval[0],maxval=pd.problemInterval[1],dtype=cfg.prec)
    return tf.stack([time,space],axis=1)

def BVPointsDomain(numPoints): #Generate points on the spatial boundary
    spaceMask = tf.random.categorical(tf.math.log([[0.5, 0.5]]), numPoints)
    spaceMask = tf.cast(spaceMask,dtype=cfg.prec)
    space = (tf.constant(pd.problemInterval[0],dtype=cfg.prec)*spaceMask +
             tf.constant(pd.problemInterval[1],dtype=cfg.prec)*(tf.constant(1.0,dtype=cfg.prec)-spaceMask))
    time = tf.random.uniform(shape=(1,numPoints),minval=pd.timeInterval[0],maxval=pd.timeInterval[1],dtype=cfg.prec)
    #print(space)
    #print(time)
    return tf.transpose(tf.reshape(tf.stack([time,space],axis=1),(2,numPoints)))

def BVPointsLeft(numPoints):#Generate Points only on the left boundary (in space) of the space-time domain
    space = tf.ones(shape=(numPoints,),dtype=cfg.prec)*tf.constant(pd.problemInterval[0],dtype=cfg.prec)
    time = tf.random.uniform(shape=(numPoints,),minval=pd.timeInterval[0],maxval=pd.timeInterval[1],dtype=cfg.prec)
    return tf.stack([time,space],axis=1)

def BVPointsRight(numPoints): #Generate Points only on the right boundary (in space) of the space-time domain
    space = tf.ones(shape=(numPoints,),dtype=cfg.prec)*tf.constant(pd.problemInterval[1],dtype=cfg.prec)
    time = tf.random.uniform(shape=(numPoints,),minval=pd.timeInterval[0],maxval=pd.timeInterval[1],dtype=cfg.prec)
    return tf.stack([time,space],axis=1)

# ------------------------------- Network training utilities
# Resets model weights to random configuration
# May be used to reset adverserial network weights (cf. arXiv:2207.08483)
# Our Training loop doesn't use this
def reinitialize(model): 
    for l in model.layers:
        if hasattr(l,"kernel_initializer"):
            l.kernel.assign(l.kernel_initializer(tf.shape(l.kernel)))
        if hasattr(l,"bias_initializer"):
            l.bias.assign(l.bias_initializer(tf.shape(l.bias)))
        if hasattr(l,"recurrent_initializer"):
            l.recurrent_kernel.assign(l.recurrent_initializer(tf.shape(l.recurrent_kernel)))